
from abc import ABC

class Animal(ABC):
	def eat(self,food):
		print(f"{self._name} has eaten {food}")
		pass
	def make_sound(self):	
		pass
	

class Cat(Animal):
	def __init__(self,name,breed,age):
		super().__init__()
		self._name = name
		self._breed = breed
		self._age = age
	def make_sound(self):
		print(f"{self._name} is hissing!")	
		
	def get_name(self):
		print(f"The cat's name is {self._name}")

	def set_name(self,name):
		self._name = name

	def get_breed(self):
		print(f"{self._name}'s breed is {self._breed}!")

	def get_age(self):
		print(f"{self._name} is {self._age} years old!")

	def set_age(self,age):
		self._age = age			

	def call(self):
		print(f"{self._name}, Come here!")				

cat1 = Cat("Puss","Persian",4)

cat1.get_name()
cat1.get_breed()
# cat1.set_name("test")
cat1.get_age()
cat1.set_age(5)
cat1.get_age()
cat1.eat("Tuna")
cat1.make_sound()
cat1.call()


class Dog(Animal):
	def __init__(self,name,breed,age):
		super().__init__()
		self._name = name
		self._breed = breed
		self._age = age
	def make_sound(self):
		print(f"{self._name} is barking!")	
		
	def get_name(self):
		print(f"The dog's name is {self._name}!")

	def set_name(self,name):
		self._name = name

	def get_breed(self):
		print(f"{self._name}'s breed is {self._breed}!")

	def get_age(self):
		print(f"{self._name} is {self._age} years old!")
	def	set_age(self,age):
		self._age = age	

	def call(self):
		print(f"Here {self._name}")

dog1 = Dog("Isis","Dalmatian",15)		

dog1.get_name()
# dog1.set_name("test2")
# dog1.get_name()
dog1.get_breed()
dog1.get_age()
dog1.set_age(16)
dog1.get_age()
dog1.eat("Steak")
dog1.make_sound()			
dog1.call()